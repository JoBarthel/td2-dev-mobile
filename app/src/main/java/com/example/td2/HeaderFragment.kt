package com.example.td2

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.example.td2.network.Api
import kotlinx.android.synthetic.main.header_fragment.*
import kotlinx.android.synthetic.main.header_fragment.view.*
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch

class HeaderFragment : Fragment()
{
    private val coroutineScope = MainScope()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

       val view = inflater.inflate(R.layout.header_fragment, container)
        coroutineScope.launch {
            val name: String? = Api.INSTANCE.userService.getInfo().body()?.firstname
            view.user_name.text = name
        }
        return view
    }

    override fun onResume() {

        MainScope().launch {
            val url = Api.INSTANCE.userService.getInfo().body()?.avatar ?: "https://goo.gl/gEgYUd"
            Glide.with(this@HeaderFragment).load(url).fitCenter().circleCrop().into(user_avatar)
        }

        user_avatar.isClickable = true
        user_avatar.setOnClickListener {
            val selectAvatarIntent = Intent(activity?.baseContext, UserInfoActivity::class.java)
            startActivity(selectAvatarIntent)
        }
        super.onResume()

    }
}