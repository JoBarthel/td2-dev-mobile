package com.example.td2.ui

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import androidx.paging.PagedList
import com.example.td2.data.TaskRepository
import com.example.td2.model.Task
import com.example.td2.model.TaskSearchResult

class TasksViewModel(private val repository: TaskRepository) : ViewModel() {

    companion object {
        private const val VISIBLE_THRESHOLD = 1
    }

    private val queryLiveData = MutableLiveData<String>()
    private val taskResult: LiveData<TaskSearchResult> = Transformations.map(queryLiveData) {
        val tasks = repository.getTasks()
        Log.d("TasksViewModel", "${tasks.data.value}")
        tasks
    }

    val tasks: LiveData<PagedList<Task>> = Transformations.switchMap(taskResult) { it.data}
    val networkErrors: LiveData<String> = Transformations.switchMap(taskResult) {it.networkErrors}

    fun searchTask() {
        queryLiveData.postValue("")
        repository.requestMore()
        Log.d("TaskViewModel", tasks.value.toString())
    }

    fun deleteTask(id: String) {
        repository.deleteTask(id)
    }

    fun addTask(task: Task) {
        repository.addTask(task)
    }

    fun updateTask(task: Task) {
        repository.updateTask(task)
    }
    fun listScrolled(visibleItemCount: Int, lastVisibleItemPosition: Int, totalItemCount: Int) {
        if (visibleItemCount + lastVisibleItemPosition + VISIBLE_THRESHOLD >= totalItemCount) {
                repository.requestMore()
        }
    }

    fun lastQueryValue(): String? = queryLiveData.value

}