package com.example.td2.data

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.paging.LivePagedListBuilder
import com.example.td2.api.TaskService
import com.example.td2.api.searchTask
import com.example.td2.db.TaskLocalCache
import com.example.td2.model.Task
import com.example.td2.model.TaskSearchResult

class TaskRepository(
    private val taskService: TaskService,
    private val cache: TaskLocalCache
) {


    // keep the last requested page. When the request is successful, increment the page number.
    private var lastRequestedPage = 1

    // LiveData of network errors.
    private val networkErrors = MutableLiveData<String>()

    // avoid triggering multiple requests in the same time
    private var isRequestInProgress = false

    // get all tasks
    fun getTasks(): TaskSearchResult {
        Log.d("TaskRepository", "New query")
        val dataSourceFactory = cache.getTasks()
        val data = LivePagedListBuilder(dataSourceFactory, DATABASE_PAGE_SIZE).build()
        val result = TaskSearchResult(data, networkErrors)
        Log.d("TaskRepository",  networkErrors.value.toString())
        return result
    }

    fun requestMore()
    {
        Log.d("TaskRepository", "requesting more")
        requestAndSaveData()
    }

    private fun requestAndSaveData() {

        if(isRequestInProgress) return
        isRequestInProgress = true

        searchTask(taskService,  lastRequestedPage, NETWORK_PAGE_SIZE, { repos ->
            cache.insert(repos) {
                lastRequestedPage++
                isRequestInProgress = false
            }
        }, { error ->
            Log.d("TaskRepository", error)
            isRequestInProgress = false
        })

    }

    fun deleteTask(id: String) {
        com.example.td2.api.deleteTask(id, taskService)
        cache.remove(id)
        getTasks()
    }

    fun addTask(task: Task) {
        com.example.td2.api.addTask(taskService, task) { getTasks()}
    }

    fun updateTask(task: Task) {
        com.example.td2.api.updateTask(task, taskService)
        cache.update(task)
        getTasks()
    }

    companion object {
        private const val NETWORK_PAGE_SIZE = 50
        private const val DATABASE_PAGE_SIZE = 20
    }
}