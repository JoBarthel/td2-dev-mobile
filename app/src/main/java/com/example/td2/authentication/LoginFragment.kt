package com.example.td2.authentication

import android.content.Intent
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.edit
import androidx.fragment.app.Fragment
import com.example.td2.MainActivity
import com.example.td2.R
import com.example.td2.SHARED_PREF_TOKEN_KEY
import com.example.td2.network.Api
import com.example.td2.network.login
import kotlinx.android.synthetic.main.fragment_login.*

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [LoginFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [LoginFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class LoginFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        login.setOnClickListener {
            tryLogIn()
        }
    }

    private fun tryLogIn() {
        if(email.text.isEmpty() || password.text.isEmpty())
            return

        val form = LoginForm(email.text.toString(), password.text.toString())
        login(Api.INSTANCE.userService, form, ::storeTokenAndDisplayTasks, ::displayError)

    }

    private fun displayError(message: String)
    {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }

    private fun storeTokenAndDisplayTasks(fetchedToken: String)
    {
        store(fetchedToken)
        val intent = Intent(activity?.applicationContext, MainActivity::class.java)
        startActivity(intent)
    }

    private fun store(fetchedToken: String)
    {
        PreferenceManager.getDefaultSharedPreferences(context).edit {
            putString(SHARED_PREF_TOKEN_KEY, fetchedToken)
        }
    }
}
