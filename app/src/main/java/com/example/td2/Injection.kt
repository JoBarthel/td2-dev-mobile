package com.example.td2

import android.content.Context
import androidx.lifecycle.ViewModelProvider
import com.example.td2.api.TaskService
import com.example.td2.data.TaskRepository
import com.example.td2.db.TaskDatabase
import com.example.td2.db.TaskLocalCache
import com.example.td2.ui.ViewModelFactory
import java.util.concurrent.Executors

object Injection {
    fun provideViewModelFactory(context: Context): ViewModelProvider.Factory {
        return ViewModelFactory(provideTaskRepository(context))
    }

    private fun provideTaskRepository(context: Context): TaskRepository {
        return TaskRepository( TaskService.create(), provideCache(context) )
    }

    private fun provideCache(context: Context): TaskLocalCache {
        val database: TaskDatabase = TaskDatabase.getInstance(context)
        return TaskLocalCache(database.taskDao(), Executors.newSingleThreadExecutor())
    }

}