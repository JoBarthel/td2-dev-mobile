package com.example.td2

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.paging.PagedList
import androidx.recyclerview.widget.DividerItemDecoration
import com.example.td2.model.Task
import com.example.td2.ui.TaskAdapter
import com.example.td2.ui.TasksViewModel
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlinx.android.synthetic.main.task_fragment.*

class MainActivity : AppCompatActivity() {

    private lateinit var viewModel: TasksViewModel
    private lateinit var adapter: TaskAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val taskFormIntent = Intent(this, TaskFormActivity::class.java)
        taskFormIntent.putExtra("isEdit", false)

        val button = findViewById<FloatingActionButton>(R.id.floatingActionButton)
        button.setOnClickListener {
            startActivity(taskFormIntent)
        }

        // get the view model
        viewModel = ViewModelProviders.of(this, Injection.provideViewModelFactory(this))
            .get(TasksViewModel::class.java)
        adapter = TaskAdapter(viewModel::deleteTask, ::onEditTask)
        // add dividers between RecyclerView's row items
        val decoration = DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
        task_view.addItemDecoration(decoration)
        setupScrollListener()

        initAdapter()
        viewModel.searchTask()
        initSearch()
    }

    private fun onEditTask(task: Task)
    {
        val taskFormIntent = Intent ( this, TaskFormActivity::class.java)
        taskFormIntent.putExtra("isEdit", true)
        taskFormIntent.putExtra("id", task.id)
        taskFormIntent.putExtra("title", task.title)
        taskFormIntent.putExtra("description", task.description)
        startActivity(taskFormIntent)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString(LAST_SEARCH_QUERY, viewModel.lastQueryValue())
    }

    private fun initAdapter() {
        task_view.adapter = adapter
        viewModel.tasks.observe(this, Observer<PagedList<Task>> {
            Log.d("Activity", "list: ${it?.size}")
            showEmptyList(it?.size == 0)
            adapter.submitList(it)
        })
        viewModel.networkErrors.observe(this, Observer<String> {
            Toast.makeText(this, "\uD83D\uDE28 Wooops $it", Toast.LENGTH_LONG).show()
        })
    }

    private fun initSearch() {
        updateRepoListFromInput()
    }

    private fun updateRepoListFromInput() {
        task_view.scrollToPosition(0)
        viewModel.searchTask()
        adapter.submitList(null)
    }

    private fun showEmptyList(show: Boolean) {
        if (show) {
            task_view.visibility = View.GONE
        } else {
            task_view.visibility = View.VISIBLE
        }
    }

    private fun setupScrollListener() {
        val layoutManager = task_view.layoutManager as androidx.recyclerview.widget.LinearLayoutManager
        task_view.addOnScrollListener(object : androidx.recyclerview.widget.RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: androidx.recyclerview.widget.RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val totalItemCount = layoutManager.itemCount
                val visibleItemCount = layoutManager.childCount
                val lastVisibleItem = layoutManager.findLastVisibleItemPosition()

                viewModel.listScrolled(visibleItemCount, lastVisibleItem, totalItemCount)
            }
        })
    }

    companion object {
        private const val LAST_SEARCH_QUERY: String = "last_search_query"
        private const val DEFAULT_QUERY = "Android"
    }
}
