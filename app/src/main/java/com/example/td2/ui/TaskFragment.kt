package com.example.td2.ui

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.paging.PagedList
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.td2.Injection
import com.example.td2.R
import com.example.td2.model.Task
import kotlinx.android.synthetic.main.task_fragment.*

class TaskFragment : Fragment()
{
    private val taskAdapter = TaskAdapter( { message -> Log.d("blah", message)}, {message -> Log.d("blah", message.id)} )
    private lateinit var viewModel: TasksViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.task_fragment, container, false)
        val recyclerView = view.findViewById<RecyclerView>(R.id.task_view) as RecyclerView
        recyclerView.adapter = taskAdapter
        recyclerView.layoutManager = LinearLayoutManager(activity)
        viewModel = ViewModelProviders.of(this, Injection.provideViewModelFactory(activity?.applicationContext!!))
            .get(TasksViewModel::class.java)

        setupScrollListener(recyclerView)
        initAdapter()
        viewModel.searchTask()
        return view
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString(LAST_SEARCH_QUERY, viewModel.lastQueryValue())
    }

    private fun initAdapter() {
        viewModel.tasks.observe(viewLifecycleOwner, Observer<PagedList<Task>> {
            Log.d("TaskFragment", "${it.size}")
            showEmptyLst(it?.size == 0)
            taskAdapter.submitList(it)
        })

        viewModel.networkErrors.observe(this, Observer<String> {
            Toast.makeText(this.context, "Network error", Toast.LENGTH_LONG).show()
        })
    }

    private fun showEmptyLst(show: Boolean) {
            task_view.visibility = View.VISIBLE
    }

    private fun setupScrollListener(view: RecyclerView) {
        val layoutManager = view.layoutManager as LinearLayoutManager
        view.addOnScrollListener(object : RecyclerView.OnScrollListener () {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val totalItemCount = layoutManager.itemCount
                val visibleItemCount = layoutManager.childCount
                val lastVisibleItem = layoutManager.findLastVisibleItemPosition()

                viewModel.listScrolled(visibleItemCount, lastVisibleItem, totalItemCount)
            }
        })
    }

    companion object {
        private const val LAST_SEARCH_QUERY: String = "last_search_query"
        private const val DEFAULT_QUERY = "Android"
    }

}