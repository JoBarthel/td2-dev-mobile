package com.example.td2.model

import androidx.lifecycle.LiveData
import androidx.paging.PagedList

data class TaskSearchResult (
    val data: LiveData<PagedList<Task>>,
    val networkErrors: LiveData<String>
)