package com.example.td2

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import com.example.td2.model.Task
import com.example.td2.ui.TasksViewModel

class TaskFormActivity : AppCompatActivity() {

    private lateinit var  titleField: TextView
    private lateinit var descField: TextView
    private lateinit  var button: Button
    private lateinit var viewModel: TasksViewModel
    private val createActivityIntent = Intent(this, MainActivity::class.java)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_task_form)

        titleField = findViewById(R.id.title)
        descField = findViewById(R.id.description)
        button = findViewById(R.id.back)
        viewModel = ViewModelProviders.of(this, Injection.provideViewModelFactory(this)).get(TasksViewModel::class.java)
        val intent = intent
        if(intent.getBooleanExtra("isEdit", false))
        {
            setEditMode()
            return
        }

        // task creation mode
        button.setOnClickListener{
            val title = titleField.text.toString()
            val description = descField.text.toString()

            viewModel.addTask(
                    Task(
                        "id_$title",
                        title,
                        description
                    )
                )
            startActivity(createActivityIntent)
        }
    }

    private fun setEditMode() {
        val intent = intent
        val id = intent.getStringExtra("id") ?: ""
        val title = intent.getStringExtra("title") ?: ""
        val description = intent.getStringExtra("description") ?: ""
        titleField.text = title
        descField.text = description

        button.setOnClickListener{
            viewModel.updateTask(
                Task(id, titleField.text.toString(),descField.text.toString())
            )
            finish()

        }
    }
}
