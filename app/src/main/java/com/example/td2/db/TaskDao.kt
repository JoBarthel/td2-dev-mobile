package com.example.td2.db

import androidx.paging.DataSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.td2.model.Task

@Dao
interface TaskDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(posts: List<Task>)

    @Query("SELECT * FROM task ORDER BY TITLE")
    fun getTasks(): DataSource.Factory<Int, Task>

    @Query("DELETE FROM task WHERE id = :id")
    fun removeTask(id: String)

    @Query("UPDATE task SET title=:title, description=:description WHERE id=:id")
    fun editTask(id: String, title: String, description: String)

}