package com.example.td2.db

import android.util.Log
import androidx.paging.DataSource
import com.example.td2.model.Task
import java.util.concurrent.Executor

class TaskLocalCache (
    private val taskDao: TaskDao,
    private val ioExecutor: Executor
) {

    fun insert(tasks: List<Task>, insertFinished: () -> Unit) {
        ioExecutor.execute{
            Log.d("TaskLocalCache", "inserting ${tasks.size} tasks")
            taskDao.insert(tasks)
            insertFinished()
        }
    }

    fun getTasks()  : DataSource.Factory<Int, Task>
    {
        val tasks = taskDao.getTasks()
        Log.d("TaskLocalCache", "getting tasks $tasks")
        return taskDao.getTasks()
    }

    fun remove(id: String) {
        ioExecutor.execute {
            taskDao.removeTask(id)
        }
    }

    fun update(task: Task) {
        ioExecutor.execute {
            taskDao.editTask(task.id, task.title, task.description ?: "")
        }
    }

}