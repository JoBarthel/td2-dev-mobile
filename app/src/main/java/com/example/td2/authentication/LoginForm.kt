package com.example.td2.authentication

data class LoginForm(val email: String, val password: String)