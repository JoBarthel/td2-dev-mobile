package com.example.td2.api
import com.example.td2.model.Task
import com.squareup.moshi.Json

data class TaskSearchResponse (
    @Json(name ="total_count") val total: Int = 0,
    @Json(name ="items") val items: List<Task> = emptyList(),
    val nextPage: Int? = null
)