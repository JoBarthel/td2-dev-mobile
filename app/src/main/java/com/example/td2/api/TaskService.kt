package com.example.td2.api

import android.util.Log
import com.example.td2.model.Task
import com.example.td2.network.Api
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.logging.HttpLoggingInterceptor.Level
import okhttp3.logging.HttpLoggingInterceptor.Logger
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

private const val TAG = "TaskService"

fun deleteTask(
    id: String,
    service: TaskService
)
{
    val request = service.deleteTask(id)
    request.enqueue(
        object: Callback<String> {
            override fun onFailure(call: Call<String>, t: Throwable) {
                Log.d(TAG, "failed to get data")
                Log.d("TaskService", t.message ?: "Unknown error")

            }

            override fun onResponse(
                call: Call<String>,
                response: Response<String>
            ) {
                Log.d(TAG, "got a response $response")
                if(response.isSuccessful) {
                    val task = response.body()
                    Log.d("TaskService", task.toString())
                } else {
                    Log.d("TaskService", response.errorBody().toString())
                }
            }
        }
    )
}

fun updateTask(
    task: Task,
    service: TaskService
){
    val request = service.updateTask(task.id, task)
    request.enqueue(
        object: Callback<Task> {
            override fun onFailure(call: Call<Task>, t: Throwable) {
                Log.d(TAG, t.message ?: "Unknown error")
            }

            override fun onResponse(call: Call<Task>, response: Response<Task>) {
               if(response.isSuccessful && response.body() != null )
                   Log.d(TAG, response.message())
                else
                   Log.d(TAG, response.errorBody().toString())
            }

        }
    )
}

fun addTask(service: TaskService, task: Task, onSuccess : (task: Task) -> Unit){
    val request = service.createTask(task)
    request.enqueue(
        object: Callback<Task> {
            override fun onFailure(call: Call<Task>, t: Throwable) {
                Log.d(TAG, t.message ?: "Unknown error")
            }

            override fun onResponse(call: Call<Task>, response: Response<Task>) {
                if(response.isSuccessful && response.body() != null)
                    onSuccess(response.body()!!)
                else
                    Log.d(TAG, response.errorBody().toString())
            }

        }
    )
}

fun searchTask(
    service: TaskService,
    page: Int,
    itemsPerPage: Int,
    onSuccess: (repos: List<Task>) -> Unit,
    onError: (error: String) -> Unit
) {
    service.getTasks(page, itemsPerPage).enqueue(
        object: Callback<List<Task>> {
            override fun onFailure(call: Call<List<Task>>, t: Throwable) {
                Log.d(TAG, "failed to get data")
                onError(t.message ?: "unknown error")
            }

            override fun onResponse(
                call: Call<List<Task>>,
                response: Response<List<Task>>
            ) {
                Log.d(TAG, "got a response $response")
                if(response.isSuccessful) {
                    val tasks = response.body()?: emptyList()
                    onSuccess(tasks)
                } else {
                    onError(response.errorBody()?.string() ?: "Unknown error")
                }
            }
        }
    )
}

interface TaskService {
    @GET("tasks")
    fun getTasks(@Query("page") page: Int,
                 @Query("per_page") itemsPerPage: Int) : Call<List<Task>>

    @DELETE("tasks/{id}")
    fun deleteTask(@Path("id") id: String): Call<String>

    @POST("tasks")
    fun createTask(@Body task: Task): Call<Task>

    @PATCH("tasks/{id}")
    fun updateTask(@Path("id") id: String, @Body task: Task): Call<Task>

    companion object {

        private const val  BASE_URL
         = "https://android-tasks-api.herokuapp.com/api/"

        fun create(): TaskService {
            val logger = HttpLoggingInterceptor( object : Logger {
                override fun log(message: String) {
                    Log.d(TAG, "message: $message")
                }

            })
            logger.level = Level.BODY

            val client = OkHttpClient.Builder()
                .addInterceptor(logger)
                .addInterceptor { chain ->
                    val newRequest = chain.request().newBuilder()
                        .addHeader("Authorization", Api.INSTANCE.getToken())
                        .build()
                    chain.proceed(newRequest)
                }
                .build()
            return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(TaskService::class.java)

        }
    }
}