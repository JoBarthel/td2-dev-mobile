package com.example.td2.ui

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.td2.R
import com.example.td2.model.Task

class TaskViewHolder(view : View) : RecyclerView.ViewHolder(view) {

    private val title: TextView = view.findViewById(R.id.task_title)
    private val description: TextView = view.findViewById(R.id.task_description)
    private val id: TextView = view.findViewById(R.id.task_id)

    fun bind(task: Task?) {

        Log.d("TaskViewHolder", task?.id ?: "no task")
        if(task == null) {
            val resources = itemView.resources
            title.text = resources.getString(R.string.loading_message)
        }
        else {
            title.text = task.title
            title.visibility = View.VISIBLE
            description.text = task.description
            description.visibility = View.VISIBLE
            id.text = task.id
            id.visibility = View.VISIBLE
        }
    }

    companion object {
        fun create(parent: ViewGroup) : TaskViewHolder {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_task, parent, false)
            return TaskViewHolder(view)
        }
    }
}
