package com.example.td2.authentication

data class TokenResponse(val token: String)