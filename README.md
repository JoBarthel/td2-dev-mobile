# Sommaire

Rendu pour le projet de l'UE développemet mobile à l'ENSIIE

La branche paging-imp a été réintégrée dans la branche master car les fonctionnalités de base ont été réparées!

# Fonctionalités 

- Ajouter, supprimer ou modifier une tâche
- Ajouter une image pour l'avatar en prenant une photo ou en utilisant une image de la mémoire du téléphone
- Se connecter avec email et mot de passe


# Implémentation du paging

Pour utiliser le paging on a du refaire complètement l'architecture de l'application en s'inspirant de https://codelabs.developers.google.com/codelabs/android-paging/

- Gson est utilisé à la place de Moshi
- L'ajout et l'édition de tâche ont été réparés et fonctionnent à nouveau !
- Les tâches sont classés par ordre alphabétiques
 
## Quelques remarques sur le projet

- L'API nous a parue très compliqué à prendre en main. L'implémentation du paging a pris 80% du temps de développement hors TP. Le reste du temps a été passé 
 à réimplémenter les fonctionalités qu'on a cassé au passage. 
- Nous avons ajouté 1000 tâches sur notre utilisateur heroku pour pouvoir tester le paging. Vous pouvez remarquer que le chargement initial de l'application est fluide malgré cela
- Nous nous sommes inspirés des ressources et des projet d'exemples disponibles ici : https://annuel.framapad.org/p/Android_Paging
- Vous pouvez remarquer que la logique de la recycler list n'est plus dans un fragment : en effet, cela posait des problèmes avec l'implémentation du paging, nous l'avons donc 
déplacé temporairement pour pouvoir avancer

## Bonus implémentés 

- TD3 : implémentation des TaskViewModel avec le ViewModelProviders