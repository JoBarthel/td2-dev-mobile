package com.example.td2.ui

import android.util.Log
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import com.example.td2.model.Task
import kotlinx.android.synthetic.main.item_task.view.*

class TaskAdapter(private val onDelete: (id: String) -> Unit,
                  private val onEdit: (task: Task) -> Unit ) : PagedListAdapter<Task, TaskViewHolder>(TASK_COMPARATOR)
{
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskViewHolder {
        return TaskViewHolder.create(parent)
    }

    override fun onBindViewHolder(holder: TaskViewHolder, position: Int) {
        val item = getItem(position) ?: return
        Log.d("ViewHolder", item.id)
        holder.bind(item)
        holder.itemView.button_delete.setOnClickListener {
            onDelete(item.id)
            notifyDataSetChanged()
        }
        holder.itemView.edit.setOnClickListener { onEdit(item) }
    }

    companion object {
        private val TASK_COMPARATOR = object : DiffUtil.ItemCallback<Task>() {
            override fun areItemsTheSame(oldItem: Task, newItem: Task): Boolean =
                oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: Task, newItem: Task): Boolean =
                oldItem == newItem
        }
    }

}